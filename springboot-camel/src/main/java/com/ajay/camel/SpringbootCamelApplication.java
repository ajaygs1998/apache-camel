package com.ajay.camel;

import com.ajay.camel.activemq.MessageProducer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class SpringbootCamelApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootCamelApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(MessageProducer producer) {
        return args -> {
            // Send a test message to the "test.queue" queue
            producer.sendMessage("test.queue", "Hello, Apache Camel with ActiveMQ!");
        };
    }

}
