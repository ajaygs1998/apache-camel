package com.ajay.camel.router;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;
import org.apache.camel.Exchange;

/**
 * FileRoute class defines a Camel route that reads files from an input directory,
 * processes their content, and writes the processed content to an output directory.
 * Additional operations include filtering files, adding headers, and moving processed files.
 */
@Component
public class FileRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        // Define source and destination directories
        String inputDir = "file://C:/data/camel/input";
        String outputDir = "file://C:/data/camel/output";
        String processedDir = "file://C:/data/camel/processed";

        // Configure the route
        from(inputDir)
                .routeId("fileRoute")
                .filter(header(Exchange.FILE_NAME).endsWith(".txt")) // Only process .txt files
                .convertBodyTo(String.class)
                .process(exchange -> {
                    String body = exchange.getIn().getBody(String.class);
                    String transformedBody = body.toUpperCase();
                    exchange.getIn().setBody(transformedBody);
                })
                .log("Processed file: ${file:name} with content: ${body}")
                .setHeader(Exchange.FILE_NAME, simple("processed-${file:name}")) // Rename processed files
                .to(outputDir)
                .to(processedDir); // Move the processed file to the processed directory
    }
}
