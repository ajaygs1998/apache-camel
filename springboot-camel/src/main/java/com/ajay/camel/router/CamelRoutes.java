package com.ajay.camel.router;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.RouteDefinition;

/**
 * A Camel route that logs a message every 5 seconds using a timer.
 */
public class CamelRoutes extends RouteBuilder {
    /**
     * Configures the Camel route.
     * <p>
     * This route uses a timer component to trigger a message every 5 seconds and
     * then logs the message using the log component.
     * </p>
     * <p>
     * The {@code from()} method is used to specify the starting point of a route, also known as an "endpoint."
     * Endpoints can be various sources of messages, such as timers, files, JMS queues, REST endpoints, and more.
     * The {@code from()} method configures the Camel component that represents the source endpoint.
     * </p>
     * <p>
     * The {@code to()} method is used to specify the destination of a message after processing it within the route.
     * The destination can be another endpoint, a processor, or a component that performs some action on the message.
     * </p>
     *
     * @throws Exception if an error occurs during route configuration
     */
    @Override
    public void configure() throws Exception {
        from("timer:hello?period=5000")
                .setBody().constant("Hello, Camel!")
                .to("log:hello");
    }

}
