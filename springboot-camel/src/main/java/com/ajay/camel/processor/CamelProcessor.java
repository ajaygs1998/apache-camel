package com.ajay.camel.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Custom processor that transforms the message body to uppercase.
 */
public class CamelProcessor implements Processor {
    /**
     * Transforms the message body to uppercase.
     *
     * @param exchange the message exchange
     * @throws Exception if an error occurs during processing
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class);
        String upperCaseBody = body.toUpperCase();
        exchange.getIn().setBody(upperCaseBody);

      
    }
}
