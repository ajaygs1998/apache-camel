package com.ajay.camel.activemq;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

/**
 * MessageProducer class to send messages to an ActiveMQ queue.
 */
@Component
public class MessageProducer {

    @Autowired
    private JmsTemplate jmsTemplate;

    /**
     * Sends a message to the specified ActiveMQ queue.
     *
     * @param destination the destination queue
     * @param message     the message to send
     */
    public void sendMessage(String destination, String message) {
        jmsTemplate.convertAndSend(destination, message);
    }
}
