package com.ajay.camel.activemq;


import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * ActiveMQRoute class to define Camel routes for processing messages from ActiveMQ.
 */
@Component
public class ActiveMQRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        // Define a route to consume messages from the "test.queue" queue
        from("activemq:queue:test.queue")
                .routeId("activeMQRoute")
                .log("Received message: ${body}")
                .process(exchange -> {
                    String body = exchange.getIn().getBody(String.class);
                    String transformedBody = body.toUpperCase();
                    exchange.getIn().setBody(transformedBody);
                })
                .log("Processed message: ${body}")
                .to("activemq:queue:test.queue.processed");
    }
}
