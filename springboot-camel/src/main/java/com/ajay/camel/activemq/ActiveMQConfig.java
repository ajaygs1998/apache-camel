package com.ajay.camel.activemq;

import org.apache.activemq.broker.BrokerService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class to set up an embedded ActiveMQ broker.
 */
@Configuration
public class ActiveMQConfig {

    /**
     * Creates and starts an embedded ActiveMQ broker.
     *
     * @return the embedded ActiveMQ broker
     * @throws Exception if the broker fails to start
     */
    @Bean
    public BrokerService broker() throws Exception {
        BrokerService broker = new BrokerService();
        broker.addConnector("tcp://localhost:61616");
        broker.setPersistent(false);
        broker.start();
        return broker;
    }
}
